// @ts-nocheck
const express = require('express')
const router = express.Router()

const { userIdProof , createUserIdProof } = require('../controllers/idProof')

router.get('/idproof' , userIdProof)
router.post('/idproof', createUserIdProof)

module.exports = router