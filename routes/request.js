// @ts-nocheck
const express = require('express')
const router = express.Router()

const {getRequest, updateRequest, getOTPEmail, submitRequest, getAllRequest} = require('../controllers/request')

router.get('/request/:id' , getRequest)
router.get('/requests' , getAllRequest)
router.post('/request' , updateRequest)
router.post('/request/otp' , getOTPEmail)
router.post('/request/submit' , submitRequest)
//router.post('/request', createRequest)

module.exports = router; 