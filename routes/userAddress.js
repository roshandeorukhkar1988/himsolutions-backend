// @ts-nocheck
const express = require('express')
const router = express.Router()

const {getUserAddress ,createUserAddress} = require('../controllers/userAddress')

router.get('/addresslist' , getUserAddress)
router.post('/addresslist' , createUserAddress )

module.exports = router; 