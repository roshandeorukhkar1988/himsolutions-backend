const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient()

exports.userIdProof = async (req, res ) =>{
    const idProof = await prisma.idProof.findMany()
    res.json({
        success : true,
        payload : idProof,
        msg : "Id added successfully"
    })
}

exports.createUserIdProof = async (req, res) => {
    let {identification_class,identification_number ,name_first ,name_last,phone_number ,selected_country_code,requestId} = req.body
    let abc;
    const creteIdProof = await prisma.idProof.create({
        data :{
            id_class     : identification_class ,
            id_number    : identification_number ,
            fName        : name_first ,
            lName        : name_last,
            phone        : phone_number ,
            country_code : selected_country_code
        }
    })

    if(requestId) {
        abc = await prisma.request.update({
         data: {
            idProofId: creteIdProof.id
         },
         where: {id: parseInt(requestId)}
       });
     }
    res.json({ success: true, payload: abc, msg: "Request Updated Successfuly"});
};