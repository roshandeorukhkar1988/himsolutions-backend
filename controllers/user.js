// @ts-nocheck
const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient()

//Get User
exports.getUser = async (req, res) => {
  const users = await prisma.user.findMany()
  res.json({
    success: true,
    payload: users,
    message: "Operation Successful",
  })
};

//Create User
exports.createUser = async (req, res) => {
  let { firstName ,lastName, email} = req.body;
  let newRequest = []
    const newUser = await prisma.user.create({
      data: {
        name: firstName + ' ' + lastName,
        email: email
      }   
    });
    if(newUser) {
       newRequest = await prisma.request.create({
        data: {
          userId: newUser.id
        }   
      });
    }
    console.log("new req", newRequest)

    res.json({ success: true, payload: newRequest, msg: "User Created Successfuly"});
};