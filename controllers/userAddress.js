const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient()

exports.getUserAddress = async (req, res) => {
  const userAddress= await prisma.addressList.findMany()
  res.json({
      success: true,
      payload: userAddress,
      message: "Operation Successful",
  })
};

exports.createUserAddress = async (req, res) => {
  let { address_city, address_country_code, address_postal_code,address_street_1,address_street_2,address_subdivision, requestId} = req.body;
  let abc;
  const createUserAddress = await prisma.addressList.create({
    data: {
      address_city         :  address_city,  
      address_country_code :  address_country_code,
      address_postal_code  :  address_postal_code,
      address_street_1      :  address_street_1 ,
      address_street_2      :  address_street_2  ,
      address_subdivision  :  address_subdivision
    }
  });

  if(requestId) {
    abc = await prisma.request.update({
     data: {
        addressId: createUserAddress.id
     },
     where: {id: parseInt(requestId)}
   });
 }
  res.json({ success: true, payload: abc, msg: "Request Updated Successfuly"});
};