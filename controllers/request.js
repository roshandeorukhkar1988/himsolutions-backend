const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient()
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey('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');

exports.getRequest = async (req, res) => {
    const {id} = req.params
    console.log("requested id", id)
    const requests = await getRequestData(id)
    res.json({
      success: true,
      payload: requests,
      message: "Request Successful",
    })
};

exports.getAllRequest = async (req, res) => {
    console.log("all req")
    const requests = await prisma.request.findMany({
        include: {
            users:true,
            idproofs: true,
            addresses: true
        }
    })
    res.json({
      success: true,
      payload: requests,
      message: "Request Successful",
    })
};



exports.updateRequest = async (req, res) => {
    const { signature, id } = req.body
    let updatedRequest = []
    updatedRequest = await prisma.request.update({
        data: {
           signature: signature
        },
        where: {id: parseInt(id)}
    });
    res.json({ success: true, payload: updatedRequest[0], msg: "Request Updated Successfuly"});
};

exports.getOTPEmail = async (req, res) => {
    const { id } = req.body
    let requestWithOTP = []
    const otpNumber = getOTPDigit()
    await prisma.request.update({
        data: {
           otp: otpNumber
        },
        where: {id: parseInt(id)}
    });
    requestWithOTP = await getRequestData(id)
    const emailData = {
        to: `${requestWithOTP.users.email}`,
        from: 'info@keasofttech.com',
        subject: `Verify your request #HIM-${requestWithOTP.id}`,
        html: `
        <p>Customer name: ${requestWithOTP.users.name}</p>
        <p>Verify with OTP.- ${otpNumber}</p>
    `
    };
    sgMail
        .send(emailData)
        .then(() => {
           return res.json({ success: true, payload: requestWithOTP, msg: "Request Updated Successfuly"});
        })
        .catch((error) => {
            return res.json({ success: false, payload: error, msg: "Request failed"});
        })
    
};
exports.submitRequest = async (req,res) => {
    const { id } = req.body
    await prisma.request.update({
        data: {
           status: "submitted"
        },
        where: {id: parseInt(id)}
    });

    const dataNew = await getRequestData(id)

    const emailData = {
        to: `${dataNew.users.email}`,
        from: 'info@keasofttech.com',
        subject: `Submitted your request #HIM-${dataNew.id}`,
        html: `
        <p>Your request submitted successfully</p>
    `
    };
    sgMail
        .send(emailData)
        .then(() => {
            return res.json({ success: true, payload: dataNew, msg: "Request Submiited Successfuly"});
        })
        .catch((error) => {
            return res.json({ success: false, payload: error, msg: "Request failed"});
        })
    
};

async function getRequestData(id) {
    const data = await prisma.request.findFirst({
        where: {
            id: parseInt(id)
        },
        include: {
            users:true,
            idproofs: true,
            addresses: true
        }
    })
    return data;
}

function getOTPDigit(){
    const otp = Math.floor(1000 + Math.random() * 9000);
    return otp
}