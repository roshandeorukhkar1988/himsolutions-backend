const express = require('express')
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const expressValidator = require('express-validator');
require('dotenv').config();

// import routes
const userRoutes = require('./routes/user');
const userAddressRoutes = require('./routes/userAddress');
const userIdProofRoutes = require('./routes/idProof');
const requestRoutes = require('./routes/request');
// app
const app = express();


// middlewares
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(expressValidator());
app.use(cors());

// routes middleware
app.use('/api',userRoutes);
app.use('/api', userAddressRoutes);
app.use('/api',userIdProofRoutes);
app.use('/api',requestRoutes);
const port = process.env.PORT || 8000;

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
